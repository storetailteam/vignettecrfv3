"use strict";

var sto = window.__sto,
  settings = require("./../../settings"),
  style = require("./main.css"),
  helper_methods = sto.utils.retailerMethod,
  placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
  classParent = "storetail-id-" + settings.format + settings.creaid,
  creaid = settings.creaid,
  companion = settings.companion != "" ? settings.companion : null,
  urlRediect = settings.redirection_url,
  targetRedirect = settings.redirection_target,
  isViewFired = false,
  isIMPfired = false,
  counter = 0,
  dataIMPTcEvents = {},
  dataVIEWTcEvents = {},
  dataCLICKTcEvents = {};

module.exports = {
  init: _init_(),
}

function resizeHomothetic() {
  try {
    var vignette = document.querySelector("." + classParent);
    if (vignette) {
      if (window.matchMedia("(max-width: 767px)").matches) {
        var h = vignette.clientWidth;
        vignette.style.height = (h / (1242 / 300)) + "px";
      } else {
        vignette.style.height = "auto";
      }
    }
  } catch (e) {

  }
}

var formatSettings = {
    "ids": [],
    "type": settings.format,
    "creaId": settings.creaid
  },
  format_setting = [{
    "id": "1111111",
    "type": "partner-card",
    "partner": {
      "name": "storetail",
      "crea_id": settings.creaid,
      "crea_type": "VI",
      "need_products": false,
      "class": "storetail storetail-vi-vignette storetail-vignette-free storetail-id-" + settings.format + settings.creaid,
      "position_reference": companion,
      "label": companion != '' ? "vignette-compagnon" : null
    },
    "html": "<div class='sto-logo'></div><button class='sto-vi-cta'>" + settings.btn_wording + "</button>"
  }];

function _init_() {

  try {

    var loadData = __sto.getTrackers("VI").length === undefined ? "VI" : ["VI", settings.creaid];

    sto.load(loadData, function (tracker) {
      helper_methods.crawlAPI(formatSettings).promise.then(function (d) {
        helper_methods.createFormat(tracker, format_setting, d);

        window.addEventListener("resize", function () {
          resizeHomothetic();
        });

        document.addEventListener("onFormatAddedInDOM", function (e) {
          try {
            var viContainer = e && e["detail"] && e["detail"]["formatNode"] ? e["detail"]["formatNode"] : null;

            if (viContainer && viContainer.className.indexOf(classParent) > -1) {
              viContainer.style.position = 'relative';
              viContainer.setAttribute("data-format", "VI");
              viContainer.setAttribute("data-creaid", creaid);

              style.use();  

              if (viContainer.style.display != "none" && isIMPfired === false) {
                tracker.display();
                if(settings.companion == ""){
                  dataIMPTcEvents = {
                    "event":"print_item",
                    "zone":"product-list",
                    "partenaire":"storetail",
                    "adtype":"VI",
                    "creation":settings.creaid,
                    "campagne":settings.oid,
                    "category":"storetail",
                    "action":"print-item",
                    "label":"vignette-fixe"
                  }
                } else {                  
                  dataIMPTcEvents = {
                    "event":"print_item",
                    "zone":"product-list",
                    "partenaire":"storetail",
                    "adtype":"VI",
                    "creation":settings.creaid,
                    "campagne":settings.oid,
                    "category":"storetail",
                    "action":"print-item",
                    "label":"vignette-compagnon",
                    "ean":companion,
                    "name":sto.module.getProductData(companion).attributes.slug,
                    "brand":sto.module.getProductData(companion).attributes.brand,
                    "price":sto.module.getProductData(companion).attributes.price.price
                  }
                }                
                tc_events_20(null,"event",dataIMPTcEvents);                
                isIMPfired = true;
              }                            

              if (isViewFired === false) {
                __sto.utils.addViewTracking("." + classParent, tracker);
                var int = window.setInterval(function() {
                  if (isElementInViewport(viContainer)) {
                    if(settings.companion == ""){
                      dataVIEWTcEvents = {
                        "event":"view_item",
                        "zone":"product-list",
                        "partenaire":"storetail",
                        "adtype":"VI",
                        "creation":settings.creaid,
                        "campagne":settings.oid,
                        "category":"storetail",
                        "action":"print-item",
                        "label":"vignette-fixe"
                      };
                    } else {
                      dataVIEWTcEvents = {
                        "event":"view_item",
                        "zone":"product-list",
                        "partenaire":"storetail",
                        "adtype":"VI",
                        "creation":settings.creaid,
                        "campagne":settings.oid,
                        "category":"storetail",
                        "action":"view-item",
                        "label":" vignette-compagnon",
                        "ean":companion,
                        "name":sto.module.getProductData(companion).attributes.slug,
                        "brand":sto.module.getProductData(companion).attributes.brand,
                        "price":sto.module.getProductData(companion).attributes.price.price
                      }
                    }
                    tc_events_20(null,"event",dataVIEWTcEvents);
                    window.clearInterval(int);
                  }
                }, 200);
                isViewFired = true;
              }              

              switch (settings.cta_type) {
                case 'redirection':
                  viContainer.classList.add('sto-cursor');
                  viContainer.addEventListener('click', function () {
                    tracker.click();
                    if(settings.companion == ""){
                      dataCLICKTcEvents = {
                        "event":"select_content",
                        "zone":"product-list",
                        "partenaire":"storetail",
                        "adtype":"VI",
                        "creation":settings.creaid,
                        "campagne":settings.oid,
                        "category":"storetail",
                        "action":"select-content",
                        "label":"vignette-fixe"
                      }
                    }else{
                      dataCLICKTcEvents = {
                        "event":"select_content",
                        "zone":"product-list",
                        "partenaire":"storetail",
                        "adtype":"VI",
                        "creation":settings.creaid,
                        "campagne":settings.oid,
                        "category":"storetail",
                        "action":"select-content",
                        "label":"vignette-compagnon",
                        "ean":companion,
                        "name":sto.module.getProductData(companion).attributes.slug,
                        "brand":sto.module.getProductData(companion).attributes.brand,
                        "price":sto.module.getProductData(companion).attributes.price.price
                      };
                    }                    
                    tc_events_20(null,"event",dataCLICKTcEvents);
                    window.setTimeout(function(){window.open(urlRediect, targetRedirect);},800);
                  });
                  break;

                case 'file':
                  viContainer.classList.add('sto-cursor');
                  viContainer.addEventListener('click', function () {
                    tracker.openPDF();
                    var pdf = require("../../img/" + settings.file);
                    window.open(pdf, "_blank");
                  });
                  break;
              }

              resizeHomothetic();
            }
          } catch (e) {

          }
        });

      }).then(null, console.log.bind(console)); //end crawl

    }); //end load
  } catch (e) {
    //console.log(e);
  }
} //end init

function isElementInViewport(el) {
  if (typeof window.jQuery === "function" && el instanceof window.jQuery) {
    el = el[0];
  }
  var rect = el.getBoundingClientRect();
  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    !(rect.top == rect.bottom || rect.left == rect.right) &&
    !(rect.height == 0 || rect.width == 0) &&
    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
    rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
  );
}
