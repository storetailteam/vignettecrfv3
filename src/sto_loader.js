var settings = require("./settings.json");
settings.btf_container_img = settings.btf_container_img === "" ? "none" : "url(./../../img/"+settings.btf_container_img+")";
settings.logo_img = settings.logo_img === "" ? "none" : "url(./../../img/"+settings.logo_img+")";
settings.btf_container_img_mob = settings.btf_container_img_mob === "" ? "none" : "url(./../../img/"+settings.btf_container_img_mob+")";
settings.btf_couleur = settings.btf_couleur === "" ? "transparent" : settings.btf_couleur;
settings.border_color = settings.border_color === "" ? "transparent" : settings.border_color;
settings.buttons_couleur = settings.buttons_couleur === "" ? "transparent" : settings.buttons_couleur;
settings.buttons_txt = settings.buttons_txt === "" ? "transparent" : settings.buttons_txt;

settings.buttons_police_size = settings.buttons_police_size === "" ? "12" : settings.buttons_police_size;
module.exports = function (source) {
    this.cacheable();
    var test = source
        .replace(/__PLACEHOLDER__/g, `${settings.format}_${settings.name}_${settings.creaid}`)
        .replace(/__CREAID__/g, settings.creaid)
        .replace(/__FORMAT__/g, settings.format)
        .replace(/__BTFCOLOR__/g,settings.btf_couleur)
        .replace(/__BORDERCOLOR__/g,settings.border_color)
        .replace(/__BUTTONTXT__/g,settings.buttons_txt)
        .replace(/__BUTTONCOLOR__/g,settings.buttons_couleur)
        .replace(/__BTF_CONTAINE_IMG__/g,settings.btf_container_img)
        .replace(/__LOGO_IMG__/g,settings.logo_img)
        .replace(/__BTF_CONTAINE_IMG_MOB__/g,settings.btf_container_img_mob);
    return test;
};
